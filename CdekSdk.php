<?php

namespace skynet\cdek;

use Exception;
use LSS\XML2Array;
use LSS\Array2XML;
use GuzzleHttp as guzzle;
use Skynetcore_Utils as utl;
use Core_Entity;

class CdekSdk
{
	const URL_DELIVERY_REQUEST_MAIN = "https://integration.cdek.ru/new_orders.php";
	const URL_CALL_COURIER_MAIN = "https://integration.cdek.ru/call_courier.php";
	const URL_STATUS_REPORT_MAIN = "https://integration.cdek.ru/status_report_h.php";
	const URL_PVZ_LIST_MAIN = "https://integration.cdek.ru/pvzlist.php";
	const URL_INFO_REQUEST_MAIN = "https://integration.cdek.ru/info_report.php";
	const URL_DELETE_ORDERS_MAIN = "https://integration.cdek.ru/delete_orders.php";
	const URL_CALCULATOR_MAIN = "http://api.cdek.ru/calculator/calculate_price_by_json.php";

	const URL_DELIVERY_REQUEST_RESERVE = "http://integration.cdek.ru/new_orders.php";
	const URL_CALL_COURIER_RESERVE = "http://integration.cdek.ru/call_courier.php";
	const URL_STATUS_REPORT_RESERVE = "http://integration.cdek.ru/status_report_h.php";
	const URL_INFO_REQUEST_RESERVE = "http://integration.cdek.ru/info_report.php";
	const URL_PVZ_LIST_RESERVE = "http://integration.cdek.ru/pvzlist.php";
	const URL_CALCULATOR_RESERVE = "http://api.cdek.ru/calculator/calculate_price_by_json.php";

	const STATUS_CREATED = 1;
	const STATUS_DELETED = 2;
	const STATUS_TAKEN_TO_SENDER_STORAGE = 3;
	const STATUS_SENT_FROM_SENDER_CITY = 6;
	const STATUS_RETURNED_TO_SENDER_STORAGE = 16;
	const STATUS_GIVEN_TO_CARRIER_IN_SENDER_CITY = 7;
	const STATUS_SENT_TO_TRANSIT_CITY = 21;
	const STATUS_MET_IN_TRANSIT_CITY = 22;
	const STATUS_TAKEN_TO_TRANSIT_STORAGE = 13;
	const STATUS_RETURNED_TO_TRANSIT_STORAGE = 17;
	const STATUS_SENT_FROM_TRANSIT_CITY = 19;
	const STATUS_GIVEN_TO_CARRIER_IN_TRANSIT_CITY = 20;
	const STATUS_SENT_TO_RECIPIENT_CITY = 8;
	const STATUS_MET_IN_RECIPIENT_CITY = 9;
	const STATUS_TAKEN_TO_DELIVERY_STORAGE = 10;
	const STATUS_TAKEN_TO_STORAGE_TILL_DEMAND = 12;
	const STATUS_GIVEN_FOR_DELIVERY = 11;
	const STATUS_RETURNED_TO_DELIVERY_STORAGE = 18;
	const STATUS_HANDED = 4;
	const STATUS_NOT_HANDED = 5;

	private $account;

	private $securePassword;

	private $decodeXml;

	/**
	 * @param $account - учетная запись. Учетная запись для интеграции не совпадает с учетной записью доступа в Личный Кабинет
	 * @param $securePassword - секретный код
	 * @param $decodeXml - конвертировать ли xml в массив
	 *
	 * @throws Exception
	 */
	public function __construct($account, $securePassword, $decodeXml = true)
	{
		if (
			is_string($account) && !empty($account)
			&& is_string($securePassword) && !empty($securePassword)
		) {
			$this->account = trim($account);
			$this->securePassword = trim($securePassword);
		} else {
			throw new Exception('Account and securePassword might be a non empty string!');
		}

		$this->decodeXml = (bool)$decodeXml;
	}

	/**
	 * Вычисляем значение поля secure
	 *
	 * @param $date - дата документа. Во всех модулях дата_время передается в формате UTC( 0000-00-00T00:00:00 ).
	 */
	private function getSecure($date)
	{
		return md5($date . '&' . $this->securePassword);
	}

	static public function getStatusesList()
	{
		return [
			self::STATUS_CREATED => [
				'name' => 'Создан',
				'comment' => 'Заказ зарегистрирован в базе данных СДЭК'
			],
			self::STATUS_DELETED => [
				'name' => 'Удален',
				'comment' => 'Заказ отменен ИМ после регистрации в системе до прихода груза на склад СДЭК в городе-отправителе'
			],
			self::STATUS_TAKEN_TO_SENDER_STORAGE => [
				'name' => 'Принят на склад отправителя',
				'comment' => 'Оформлен приход на склад СДЭК в городе-отправителе'
			],
			self::STATUS_SENT_FROM_SENDER_CITY => [
				'name' => 'Выдан на отправку в г.-отправителе',
				'comment' => 'Оформлен расход со склада СДЭК в городе-отправителе. Груз подготовлен к отправке (консолидирован с другими посылками)'
			],
			self::STATUS_RETURNED_TO_SENDER_STORAGE => [
				'name' => 'Возвращен на склад отправителя',
				'comment' => 'Повторно оформлен приход в городе-отправителе (не удалось передать перевозчику по какой-либо причине)'
			],
			self::STATUS_GIVEN_TO_CARRIER_IN_SENDER_CITY => [
				'name' => 'Сдан перевозчику в г.-отправителе',
				'comment' => 'Зарегистрирована отправка в городе-отправителе. Консолидированный груз передан на доставку (в аэропорт/загружен машину)'
			],
			self::STATUS_SENT_TO_TRANSIT_CITY => [
				'name' => 'Отправлен в г.-транзит',
				'comment' => 'Зарегистрирована отправка в город-транзит. Проставлены дата и время отправления у перевозчика'
			],
			self::STATUS_MET_IN_TRANSIT_CITY => [
				'name' => 'Встречен в г.-транзите',
				'comment' => 'Зарегистрирована встреча в городе-транзите'
			],
			self::STATUS_TAKEN_TO_TRANSIT_STORAGE => [
				'name' => 'Принят на склад транзита',
				'comment' => 'Оформлен приход в городе-транзите'
			],
			self::STATUS_RETURNED_TO_TRANSIT_STORAGE => [
				'name' => 'Возвращен на склад транзита',
				'comment' => 'Повторно оформлен приход в городе-транзите (груз возвращен на склад)'
			],
			self::STATUS_SENT_FROM_TRANSIT_CITY => [
				'name' => 'Выдан на отправку в г.-транзите',
				'comment' => 'Оформлен расход в городе-транзите'
			],
			self::STATUS_GIVEN_TO_CARRIER_IN_TRANSIT_CITY => [
				'name' => 'Сдан перевозчику в г.-транзите',
				'comment' => 'Зарегистрирована отправка у перевозчика в городе-транзите'
			],
			self::STATUS_SENT_TO_RECIPIENT_CITY => [
				'name' => 'Отправлен в г.-получатель',
				'comment' => 'Зарегистрирована отправка в город-получатель, груз в пути'
			],
			self::STATUS_MET_IN_RECIPIENT_CITY => [
				'name' => 'Встречен в г.-получателе',
				'comment' => 'Зарегистрирована встреча груза в городе-получателе'
			],
			self::STATUS_TAKEN_TO_DELIVERY_STORAGE => [
				'name' => 'Принят на склад доставки',
				'comment' => 'Оформлен приход на склад города-получателя, ожидает доставки до двери'
			],
			self::STATUS_TAKEN_TO_STORAGE_TILL_DEMAND => [
				'name' => 'Принят на склад до востребования',
				'comment' => 'Оформлен приход на склад города-получателя. Доставка до склада, посылка ожидает забора клиентом - покупателем ИМ'
			],
			self::STATUS_GIVEN_FOR_DELIVERY => [
				'name' => 'Выдан на доставку',
				'comment' => 'Добавлен в курьерскую карту, выдан курьеру на доставку'
			],
			self::STATUS_RETURNED_TO_DELIVERY_STORAGE => [
				'name' => 'Возвращен на склад доставки',
				'comment' => 'Оформлен повторный приход на склад в городе-получателе. Доставка не удалась по какой-либо причине, ожидается очередная попытка доставки'
			],
			self::STATUS_HANDED => [
				'name' => 'Вручен',
				'comment' => 'Успешно доставлен и вручен адресату'
			],
			self::STATUS_NOT_HANDED => [
				'name' => 'Не вручен',
				'comment' => 'Покупатель отказался от покупки, возврат в ИМ'
			]
		];
	}

	static public function getRates()
	{
		$oRate = Core_Entity::factory('Cdek_Rate');

		$oRate
			->queryBuilder()
			->orderBy('id');

		$aRates = $oRate->findAll();

		$aReturn = array(' … ');

		foreach ($aRates as $oRateItem) {
			$aReturn[$oRateItem->id] = $oRateItem->name;
		}

		return $aReturn;
	}

	static public function getCompanies()
	{
		$oCompany = Core_Entity::factory('Company');

		$oCompany
			->queryBuilder()
			->orderBy('id');

		$aCompanies = $oCompany->findAll();

		$aReturn = array(' … ');

		foreach ($aCompanies as $oCompany) {
			$aReturn[$oCompany->id] = $oCompany->name;
		}

		return $aReturn;
	}

	/**
	 * Предоставляет список ПВЗ, действующих на момент запроса
	 *
	 * @param string $url - урл, на который будем стучать
	 * @param null $cityId - код города по базе СДЭК
	 * @param null $cityPostCode - почтовый индекс города, для которого необходим список ПВЗ
	 *                             Оба параметра необязательны, если указаны оба приоритет отдается cityid.
	 *                             При отсутствии обоих параметров список ПВЗ содержит данные по всем городам
	 *
	 * @return object
	 * @throws Exception
	 */
	public function pvzList($cityId = null, $cityPostCode = null, $url = self::URL_PVZ_LIST_MAIN)
	{
		$aPvzList = array();
		$query = [];
		if (!empty($cityId)) {
			$query['cityid'] = $cityId;
		}
		if (!empty($cityPostCode)) {
			$query['citypostcode'] = $cityPostCode;
		}
		$oPvzResponse = $this->call('GET', $url, true, null, $query);
		if (isset($oPvzResponse->result['PvzList']['Pvz'])) {
			if (isset($oPvzResponse->result['PvzList']['Pvz']['WorkTimeY'])) {
				$aPvzList[] = $oPvzResponse->result['PvzList']['Pvz'];
			} else {
				$aPvzList = $oPvzResponse->result['PvzList']['Pvz'];
			}
		}

		return $aPvzList;
	}

	public function calcDelivery($order, $sendCityCodeID, $recCityCodeID, $tariffId,
								 $weight = 0,
								 $length = 0,
								 $width = 0,
								 $height = 0
	)
	{
		$url = self::URL_CALCULATOR_MAIN;
		$dateString = \Core_Date::sql2date($order->datetime);

		$orderGoods = array();
		$orderItems = $order->shop_order_items->findAll();
		/** @var Shop_Order_Item_Model $orderItem */
		$orderGoods[] = array(
			"weight" => $weight,
			"length" => $length,
			"width" => $width,
			"height" => $height,
		);
		$body = array(
			"version" => "1.0",
			"dateExecute" => $dateString,
			"authLogin" => $this->account,
			"secure" => $this->getSecure($dateString),
			"senderCityId" => $sendCityCodeID,
			"receiverCityId" => $recCityCodeID,
			"tariffId" => $tariffId,
			"goods" => $orderGoods
		);

		$sdekResponse = $this->call('POST', $url, $this->decodeXml, $body, array(), 'json');
		return $sdekResponse;
	}

	public function getOrderStatus($order, $dispatchNumber, $url = self::URL_STATUS_REPORT_MAIN)
	{

		$dateString = $order->datetime;

		$body = array('StatusReport' => array(
			'@attributes' => array(
				'Date' => $dateString,
				'Account' => $this->account,
				'Secure' => $this->getSecure($dateString),
				'ShowHistory' => "1",
				'ShowReturnOrder' => "1",
				'ShowReturnOrderHistory' => "1",
			),
//			'ChangePeriod' => array(
//				'@attributes' => array(
//					'DateFirst' => $dateString,
//				)
//			),
			'Order' => array(
				'@attributes' => array(
					'DispatchNumber' => $dispatchNumber,
//					'Number' => $order->id,
//					'Date' => $dateString,
				)
			)
		)
		);

		$statusReturn = array();
		$sdekResponse = $this->call('POST', $url, true, $body);

//		$body = Array2XML::createXML(array_keys($aBody)[0], array_values($aBody)[0])->saveXML();

		if (isset($sdekResponse->result['StatusReport']['Order']['Status'])) {
			$orderStatus = $sdekResponse->result['StatusReport']['Order']['Status'];

			if (isset($orderStatus['State'])) {
				$arOrderStatus = $orderStatus['State'];
				$statusReturn = array_pop($arOrderStatus);
			}
		} elseif (isset($sdekResponse->result['StatusReport']['Order'][1]['Status'])) {
			$orderStatus = $sdekResponse->result['StatusReport']['Order'][1]['Status'];

			if (isset($orderStatus['State'])) {
				$arOrderStatus = $orderStatus['State'];
				$statusReturn = array_pop($arOrderStatus);
			}
		}
		return $statusReturn;
	}



	public function getOrderInfo($order, $dispatchNumber, $url = self::URL_INFO_REQUEST_MAIN)
	{

		$dateString = $order->datetime;

		$body = array('InfoRequest' => array(
			'@attributes' => array(
				'Date' => $dateString,
				'Account' => $this->account,
				'Secure' => $this->getSecure($dateString),
				'ShowHistory' => "1",
				'ShowReturnOrder' => "1",
				'ShowReturnOrderHistory' => "1",
			),
			'Order' => array(
				'@attributes' => array(
					'DispatchNumber' => $dispatchNumber,
				)
			)
		)
		);

		$statusReturn = array();
		$sdekResponse = $this->call('POST', $url, true, $body);


		if (isset($sdekResponse->result['InfoReport']['Order'])) {
			return $sdekResponse->result['InfoReport']['Order'];
		}

		return false;
	}

	public function ordersPackagesPrint($order, $dispatchNumber, $url = 'https://integration.cdek.ru/ordersPackagesPrint')
	{

		$dateString = $order->datetime;

		$body = array('OrdersPackagesPrint' => array(
			'@attributes' => array(
				'Date' => $dateString,
				'Account' => $this->account,
				'Secure' => $this->getSecure($dateString),
				'OrderCount' => 1,
				'printFormat' => "A4"
			),
			'Order' => array(
				'@attributes' => array(
					'DispatchNumber' => $dispatchNumber
				)
			)
		)
		);

		$client = new guzzle\Client();
		$body = Array2XML::createXML(array_keys($body)[0], array_values($body)[0])->saveXML();
		$response = $client->post(
			$url
			, array('form_params' => array('xml_request' => $body))
		);

		file_put_contents(CMS_FOLDER.'hostcmsfiles/pdf/'.$dispatchNumber.'.pdf', $response->getBody()->getContents());

		return true;
	}

	public function ordersPrint($order, $dispatchNumber, $url = 'https://integration.cdek.ru/orders_print.php')
	{

		$dateString = $order->datetime;

		$body = array('OrdersPrint' => array(
			'@attributes' => array(
				'Date' => $dateString,
				'Account' => $this->account,
				'Secure' => $this->getSecure($dateString),
				'OrderCount' => 1,
			),
			'Order' => array(
				'@attributes' => array(
					'DispatchNumber' => $dispatchNumber
				)
			)
		)
		);

		$client = new guzzle\Client();
		$body = Array2XML::createXML(array_keys($body)[0], array_values($body)[0])->saveXML();
		$response = $client->post(
			$url
			, array('form_params' => array('xml_request' => $body))
		);

		file_put_contents(CMS_FOLDER.'hostcmsfiles/pdf/o'.$dispatchNumber.'.pdf', $response->getBody()->getContents());

		return true;
	}

	public function infoRequest($order, $dispatchNumber, $url = self::URL_STATUS_REPORT_MAIN){

		$dateString = $order->datetime;

		$body = array('InfoRequest' => array(
			'@attributes' => array(
				'Date' => $dateString,
				'Account' => $this->account,
				'Secure' => $this->getSecure($dateString),
			),
			'Order' => array(
				'@attributes' => array(
					'DispatchNumber' => $dispatchNumber,
				//	'Number' => $order->id,
				//	'Date' => $dateString,
				)
			)
		)
		);

		$statusReturn = array();
		$sdekResponse = $this->call('POST', $url, true, $body);

		utl::p($sdekResponse);

		return $statusReturn;
	}


	public function newOrders($order,
			$sendCityCodeID,
			$recCityCodeID,
			$tariffTypeCode,
			$pvzCodeId,
			$recName,
			$recPhone,
			$recMail,
			$recComment,
			$recIndex,
			$recStreet,
			$recHouse,
			$recFlat,
			$weight,
			$length,
			$width,
			$height,
			$sellerName,
			$deliveryCost,
			$url = self::URL_DELIVERY_REQUEST_MAIN) {
			$dateString = date('Y-m-d'); // $order->datetime;


		$oProperty = Core_Entity::factory('Property',638);
		$aPropertyValues = $oProperty->getValues($order->id);
		$paid = isset($aPropertyValues[0]) ? $aPropertyValues[0]->value : 0;
		$paid_orig = isset($aPropertyValues[0]) ? $aPropertyValues[0]->value : 0;

		$total = 0;

		$orderItems = array();
		$aOrderItems = $order->shop_order_items->findAll();
		/** @var Shop_Order_Item_Model $orderItem */
		foreach ($aOrderItems as $orderItem) {
			if($orderItem->type==0) {
				$warekey = (trim($orderItem->marking)=='') ? $orderItem->id.$orderItem->shop_item->id : trim($orderItem->marking);
				$quant = $orderItem->quantity*1;
				$pay = $orderItem->getPrice() * $quant;
				if ($paid > 0) {
					if ($paid >= $orderItem->getPrice() * $quant) {
						$paid = $paid - $orderItem->getPrice() * $quant;
						$pay = $pay - $orderItem->getPrice() * $quant;
					} else {
						$pay = $pay - $paid;
						$paid = 0;
					}
				}
				$total = $total + $orderItem->getPrice() * $quant;

				$orderItems[] = array(
					'@attributes' => array(
						'WareKey' => $warekey,
						'Cost' => $orderItem->getPrice(), // ->getAmount()/$quant,
						'PaymentVATRate' => 'VATX',
						'Payment' => $pay/$quant, // getAmount()/$quant,
						'Weight' => $orderItem->shop_item->weight*1000,
						'Amount' => $quant,
						'Comment' => $orderItem->name,
					)
				);
			}
		}


		$pay = $deliveryCost;
		if ($paid > 0) {
			if ($paid >= $deliveryCost) {
				$paid = $paid - $deliveryCost;
				$pay = $pay - $deliveryCost;
			} else {
				$pay = $pay - $paid;
				$paid = 0;
			}
		}
		$total = $total + $deliveryCost;

		if ($paid_orig > $total) {
			\Core_Message::show('Оплаченная сумма по заказу больше суммы заказа!', 'error');
			return false;
		}
		

		$body = array('DeliveryRequest' => array(
				'@attributes' => array(
					'Number' => $order->id,
					'Date' => $dateString,
					'Account' => $this->account,
					'Secure' => $this->getSecure($dateString),
					'OrderCount' => "1"
				),
			'Order' => array(
				'@attributes' => array(
					'Number' => $order->id, // '1', // microtime(),
					'SendCityCode' => $sendCityCodeID,
					'RecCityCode' => $recCityCodeID,
					'RecipientName' => $recName,
					'RecipientEmail' => $recMail,
					'Phone' => $recPhone,
					'Comment' => $recComment,
					'TariffTypeCode' => $tariffTypeCode,
					'DeliveryRecipientCost' => $pay,
					'RecientCurrency' => "RUB",
					'ItemsCurrency' => "RUB",
					'SellerName' => $sellerName
				),
				'Address' => array(
					'@attributes' => array(
						'Street' => $recStreet,
						'House' => $recHouse,
						'Flat' => $recFlat,
						'PvzCode' => $pvzCodeId,
					)
				),
				'Package' => array(
					'@attributes' => array(
						'Number' => 1,
						'BarCode' => 1,
						'Weight' => $weight*1,
						'SizeA' => $length*1,
						'SizeB' => $width*1,
						'SizeC' => $height*1,
					),
					'Item' => $orderItems,
					)
				),
			)
		);

		$sdekResponse = $this->call('POST', $url, true, $body);
		if(isset($sdekResponse->result['response']['Order'][0]['@attributes']['DispatchNumber'])) {
			// Объект дополнительного свойства с идентификатором 123
			$oProperty = Core_Entity::factory('Property')->getByGuid('trek_number_'.$order->shop->site->id);
			// Массив значений свойства 123 для информационного элемента $informationsystem_item_id
			$aPropertyValues = $oProperty->getValues($order->id);
			if(!isset($aPropertyValues[0])) {
				$aPropertyValue = $oProperty->createNewValue($order->id);
			} else {
				$aPropertyValue = $aPropertyValues[0];
			}
			$aPropertyValue->value = $sdekResponse->result['response']['Order'][0]['@attributes']['DispatchNumber'];
			$aPropertyValue->save();

			\Core_Message::show("Заказ №{$aPropertyValue->value}, СДЭК создан!", 'message');

			$prevStatus = $order->shop_order_status;
			$prevStatusName = isset($prevStatus->name) ? $prevStatus->name : '...';
			/* Меняем стату на "в сборке" */
			$orderStatus = Core_Entity::factory('Shop_Order_Status')->getById(13);
			$order->shop_order_status_id = $orderStatus->id;
			$order->save();
			$statusComment = "Изменение статуса заказа {$order->id} с '{$prevStatusName}' на '{$orderStatus->name}'";

			/* Пишем в комментарий заказа*/
			$oOrderComment = Core_Entity::factory('Forbiker_Shop_Order_Comment');
			$oOrderComment->writeComment(
				$order->id,
				$order->siteuser_id,
				$statusComment,
				$prevStatus->id
			);

		}

		return $sdekResponse;
	}

	/** Общий метод для отправки запроса
	 *
	 * @param       $method - метод (GET, POST, ....)
	 * @param       $url - на какой url отправлять запрос
	 * @param       $decodeXml - конвертировать ли xml в массив
	 * @param mixed $body - тело запроса (для метода POST)
	 * @param array $query - параметры GET запроса
	 *
	 * @return object
	 * @throws Exception
	 */
	protected function call($method, $url, $decodeXml = true, $aBody = null, array $query = [], $type='xml')
	{
		try {
			$client = new guzzle\Client();

			$response = NULL;
			switch ($type) {
				case 'xml':
					switch ($method) {
						case 'GET':
							$response = $client->get(
								$url
								, array('query' => $query)
							); // createRequest($method, $url, null, $body, ['query' => $query]);
							break;
						case 'POST':
							$body = Array2XML::createXML(array_keys($aBody)[0], array_values($aBody)[0])->saveXML();
							$response = $client->post(
								$url
								, array('form_params' => array('xml_request' => $body))
							);
							break;
					}
					break;
				case 'json':
					$response = $client->post(
						$url
						, array(
							'content-type' => 'application/json',
							'body' => json_encode($aBody)
						)
					);
					break;
			}

			if (is_object($response) && $response->getStatusCode()==200) {
				$rBody = $response->getBody();

				if (!$rBody->getSize()) {
					return (object)['result' => null, 'url' => $url];
				}
				$bodyContent = $rBody->getContents();

				switch ($type) {
					case 'xml':
						$aResponse = XML2Array::createArray($bodyContent);
						//т.к. апи даже в случае ошики отдает 200, то сами проверяем на наличие ошибки
						if (isset($aResponse['response']) || isset($aResponse['StatusReport'])) {
							if (isset($aResponse['StatusReport'])) {
								if (isset($aResponse['StatusReport']['@attributes']['ErrorCode'])) {
									throw new Exception($aResponse['StatusReport']['@attributes']['ErrorCode'] . ': ' . $aResponse['StatusReport']['@attributes']['Msg']);
								}
							} else {
								$aResponseValues = array_values($aResponse['response']);

								if (isset($aResponseValues[0]) && isset($aResponseValues[0]['@attributes']) && isset($aResponseValues[0]['@attributes']['ErrorCode'])) {
									throw new Exception($aResponseValues[0]['@attributes']['ErrorCode'] . ': ' . $aResponseValues[0]['@attributes']['Msg']);
								} elseif (isset($aResponseValues[0][0]) && isset($aResponseValues[0][0]['@attributes']) && isset($aResponseValues[0][0]['@attributes']['ErrorCode'])) {
									throw new Exception($aResponseValues[0][0]['@attributes']['ErrorCode'] . ': ' . $aResponseValues[0][0]['@attributes']['Msg']);
								}
							}
						}
						if ($decodeXml) {
							$result = (object)[
								'result' => $aResponse,
								'url' => $url
							];
						} else {
							$result = (object)['result' => $bodyContent, 'url' => $url];
						}
						break;
					case 'json':
						$arJSON = json_decode($bodyContent, true);

						if(isset($arJSON['error'])) {
							throw new Exception($arJSON['error'][0]['code'].': '.$arJSON['error'][0]['text']);
						} else {
							$result = (object)['result' => $arJSON['result'], 'url' => $url];
						}
						break;
				}
				return $result;
			}

			throw new Exception($response->getMessage());
		} catch (Exception $e) {
			if ($e->getMessage() != '3: Невозможно осуществить доставку по этому направлению при заданных условиях') {
				\Core_Message::show($e->getMessage(), 'error');
			}
		}
	}
}
