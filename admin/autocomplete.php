<?php
use Skynetcore_Utils as utl;
require_once(dirname(__FILE__) . '/../../../../' . 'bootstrap.php');

defined('HOSTCMS') || exit('HostCMS: access denied.');

header('Content-type: application/json; charset=utf-8');

$returnes = array();
switch (Core_Array::getRequest('action')) {
	case 'get_pvz_list':
		$config = Core_Config::instance()->get('site_config', array())[Core_Array::getRequest('site', 0)];
		$configCdek = $config['cdek'][0];
		$sdekInstance = new skynet\cdek\CdekSdk($configCdek['login'], $configCdek['secure']);
		$aPvzList = $sdekInstance->pvzList(Core_Array::getRequest('city_code', 0));

		$returnes[] = array(
			'id' => 'none',
			'name' => "...",
		);
		foreach ($aPvzList as $aPvzListItem) {
			$returnes[] = array(
				'id' => $aPvzListItem['@attributes']['Code'],
				'name' => "[{$aPvzListItem['@attributes']['Code']}] {$aPvzListItem['@attributes']['FullAddress']}, {$aPvzListItem['@attributes']['Code']}",
			);
		}
		break;
	default:
		$qs = Core_Array::getGet('queryString', 'not-defined');
		$oCdekCities = Core_Entity::factory('Cdek_City');
		$oCdekCities
			->queryBuilder()
			->where('city_name', 'LIKE', "{$qs}%");
		$aCdekCities = $oCdekCities
			->findAll(FALSE);

		$returnes = array();
		foreach ($aCdekCities as $aCdekCity) {
			$returnes[] = array(
				'id' => $aCdekCity->id,
				'label' => "[$aCdekCity->id] $aCdekCity->full_name",
			);
		}
}

echo json_encode($returnes);